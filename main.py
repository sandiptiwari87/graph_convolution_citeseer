#!/usr/bin/env python2
# Team : Support_Vectors 
# Sandip Tiwari (20162032)
# Nagaraj Poti (20162010)
# Vatsal Nagda (20162008)
# Sachin Lomte (20162065)

from __future__ import division
from __future__ import print_function
from utils import *
from convolutions import *
import os
import warnings
import time
import tensorflow as tf
from random import randint
import matplotlib.pyplot as plt

# Disable CPU warnings made by the Tensorflow library
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
# Disable deprecation warnings 
warnings.filterwarnings("ignore")

# Set random seed for the numpy and tensor flow operations
seed = randint(10, 1000)
np.random.seed(seed)
tf.set_random_seed(seed)

# Hyper-parameter definitions
param_learning_rate = 0.01       # Initial learning rate
param_epochs = 200               # Number of epochs to train.
param_hidden1 = 16               # Number of units in hidden layer 1.
param_dropout = 0.5              # Dropout rate (1 - keep probability).
param_weight_decay = 5e-4        # Weight for L2 loss on embedding matrix.

# Load data from data files 
# ind.citeseer.{} { 'x', 'y', 'tx', 'ty', 'allx', 'ally', 'graph' }
adj, features, y_train, y_val, y_test, train_mask, val_mask, test_mask = load_data()

# Some preprocessing
features = preprocess_features(features)
support = [preprocess_adj(adj)]

# Define placeholders for future computations
placeholders = {
    'support': [tf.sparse_placeholder(tf.float32)],
    'features': tf.sparse_placeholder(tf.float32, shape=tf.constant(features[2], dtype=tf.int64)),
    'labels': tf.placeholder(tf.float32, shape=(None, y_train.shape[1])),
    'labels_mask': tf.placeholder(tf.int32),
    'dropout': tf.placeholder_with_default(0., shape=()),
    'num_features_nonzero': tf.placeholder(tf.int32)  # helper variable for sparse dropout
}

# Create model
def mk_model(input_dim):
    name = "my_model"
    vars = {}
    layers = []
    activations = []
    loss = 0
    accuracy = 0
    inputs = placeholders['features']
    input_dim = input_dim
    outputs = None
    output_dim = placeholders['labels'].get_shape().as_list()[1]

		# Optimizer that implements the Adam algorithm (Kingma's paper)
		# Method for efficient stochastic optimization that only requires
		# first-order gradients with little memory requirement
    optimizer = tf.train.AdamOptimizer(learning_rate=param_learning_rate)

    with tf.variable_scope(name):
				# Definition of the input -> hidden layer
        layers.append(GraphConvolution(input_dim=input_dim,
                                       output_dim=param_hidden1,
                                       placeholders=placeholders,
                                       act=tf.nn.relu, # Relu activation function
                                       sparse_inputs=True))
				
				# Definition of the hidden -> output layer
        layers.append(GraphConvolution(input_dim=param_hidden1,
                                       output_dim=output_dim,
                                       placeholders=placeholders,
                                       act=lambda x: x)) # Identity function

    # Build sequential layer model
    activations.append(inputs)
    for layer in layers:
        hidden = layer(activations[-1], placeholders)
        activations.append(hidden)
    outputs = activations[-1]

    # Store model variables for easy access
    variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=name)
    vars = {var.name: var for var in variables}

    # Build metrics
    # Weight decay loss
    for var in layers[0].vars.values():
        loss += param_weight_decay * tf.nn.l2_loss(var)

    # Cross entropy error
    loss += masked_softmax_cross_entropy(outputs, placeholders['labels'],
                                              placeholders['labels_mask'])

    accuracy = masked_accuracy(outputs, placeholders['labels'],
                                    placeholders['labels_mask'])

    opt_op = optimizer.minimize(loss)
    return loss, accuracy, opt_op


model_loss, model_accuracy, model_opt_op = mk_model(features[2][1]);

# Initialize session
sess = tf.Session()

# Define model evaluation function
def evaluate(features, support, labels, mask, placeholders):
    t_test = time.time()
    feed_dict_val = construct_feed_dict(features, support, labels, mask, placeholders)
    outs_val = sess.run([model_loss, model_accuracy], feed_dict=feed_dict_val)
    return outs_val[0], outs_val[1], (time.time() - t_test)


# Init variables
sess.run(tf.global_variables_initializer())
cost_val = []
current_time = time.time()
# For printing graph
epoch_x = [] 
accuracy_out = []

# Train model
for epoch in range(param_epochs):

    # Construct feed dictionary
    feed_dict = construct_feed_dict(features, support, y_train, train_mask, placeholders)
    feed_dict.update({placeholders['dropout']: param_dropout})

    # Training step
    outs = sess.run([model_opt_op, model_loss, model_accuracy], feed_dict=feed_dict)

    # Validation
    cost, acc, duration = evaluate(features, support, y_val, val_mask, placeholders)
    cost_val.append(cost)

    print("===== Epoch : %d =====" % (epoch + 1))
    print("Loss incurred during training : %.4f" % outs[1])
    print("Training accuracy : %.4f" % outs[2])
    print("Time : %.2f" % (time.time() - current_time))
    epoch_x.append(epoch + 1)
    accuracy_out.append(outs[2]*100)

print("===== TRAINING COMPLETE =====\n\n")

# Print graph
# Plot accuracy values
vot = plt.scatter([i for i in range(1, 201)], accuracy_out, c = "red", label = "Citeseer") 
vot = plt.plot([i for i in range(1, 201)], accuracy_out, c = "red")
plt.xticks(np.arange(0, 210, 10))
plt.yticks(np.arange(0, 105, 5))
plt.suptitle("Graph Convolutional Networks - Training accuracy", fontsize = 18)
plt.xlabel("Epochs", fontsize = 16)
plt.ylabel("Accuracy %", fontsize = 16)
plt.legend(loc = 'center right', shadow = True)
plt.show()

# Testing
test_cost, test_acc, test_duration = evaluate(features, support, y_test, test_mask, placeholders)
print("=====TEST SET RESULTS  =====")
print("Test cost : %.4f" % test_cost)
print("Test accuracy : %.4f" % (test_acc * 100))
print("Time : %.2f" % test_duration)
