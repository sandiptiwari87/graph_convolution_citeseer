# Team : Support_Vectors 
# Sandip Tiwari (20162032)
# Nagaraj Poti (20162010)
# Vatsal Nagda (20162008)
# Sachin Lomte (20162065)

import numpy as np
import scipy.sparse as sp
from scipy.sparse.linalg.eigen.arpack import eigsh
import networkx as nx
import tensorflow as tf
import pickle 
import sys

# Parse the test index file
def parse_index_file(filename):
    index = []
    for line in open(filename):
        index.append(int(line.strip()))
    return index


# Prepare a boolean array consisting of one 1 at a specified index 
# and rest initialized to zeroes
def mask_to_array(idx, l):
    mask = np.zeros(l)
    mask[idx] = 1
    return np.array(mask, dtype=np.bool)


# Initialize array to zeros
def zeros(shape, name=None):
    initial = tf.zeros(shape, dtype=tf.float32)
    return tf.Variable(initial, name=name)


# Load data from dataset files. Dataset files are in binary format.
# Pickle library used to load binary files
def load_data():
    # Retrieve the filenames with the following suffixes
    names = ['x', 'y', 'tx', 'ty', 'allx', 'ally', 'graph']
    objects = []
    for i in range(len(names)):
        with open("data/ind.citeseer.{}".format(names[i]), 'rb') as f:
            # Load data with pickle binary file loader
            objects.append(pickle.load(f))

    # Tuples corresponding to the files loaded
    x, y, tx, ty, allx, ally, graph = tuple(objects)
    test_idx_reorder = parse_index_file("data/ind.citeseer.test.index")
    test_idx_range = np.sort(test_idx_reorder)

    # Fix citeseer dataset (there are some isolated nodes in the graph)
    # Find isolated nodes, add them as zero-vecs into the right position
    test_idx_range_full = range(min(test_idx_reorder), max(test_idx_reorder)+1)
    tx_extended = sp.lil_matrix((len(test_idx_range_full), x.shape[1]))
    tx_extended[test_idx_range-min(test_idx_range), :] = tx
    tx = tx_extended
    ty_extended = np.zeros((len(test_idx_range_full), y.shape[1]))
    ty_extended[test_idx_range-min(test_idx_range), :] = ty
    ty = ty_extended

    features = sp.vstack((allx, tx)).tolil()
    features[test_idx_reorder, :] = features[test_idx_range, :]
    adj = nx.adjacency_matrix(nx.from_dict_of_lists(graph))

    labels = np.vstack((ally, ty))
    labels[test_idx_reorder, :] = labels[test_idx_range, :]

    idx_test = test_idx_range.tolist()
    idx_train = range(len(y))
    idx_val = range(len(y), len(y)+500)

    train_mask = mask_to_array(idx_train, labels.shape[0])
    val_mask = mask_to_array(idx_val, labels.shape[0])
    test_mask = mask_to_array(idx_test, labels.shape[0])

    y_train = np.zeros(labels.shape)
    y_val = np.zeros(labels.shape)
    y_test = np.zeros(labels.shape)
    y_train[train_mask, :] = labels[train_mask, :]
    y_val[val_mask, :] = labels[val_mask, :]
    y_test[test_mask, :] = labels[test_mask, :]

    return adj, features, y_train, y_val, y_test, train_mask, val_mask, test_mask


# Convert a sparse matrix into tuple representation format
def sparsematrix_to_tuple(sparse_mx):
    mx = sparse_mx
    if not sp.isspmatrix_coo(mx):
        # Convert to coordinate format
        mx = sparse_mx.tocoo()
    coords = np.vstack((mx.row, mx.col)).transpose()
    return coords, mx.data, mx.shape


# Symmetrically normalize adjacency matrix
def normalize_adj(adj):
    adj = sp.coo_matrix(adj)
    rowsum = np.array(adj.sum(1))
    d_inv_sqrt = np.power(rowsum, -0.5).flatten()
    d_inv_sqrt[np.isinf(d_inv_sqrt)] = 0.
    d_mat_inv_sqrt = sp.diags(d_inv_sqrt, 0)
    return adj.dot(d_mat_inv_sqrt).transpose().dot(d_mat_inv_sqrt).tocoo()


# Row-normalize feature matrix and convert to tuple representation
def preprocess_features(features):
    # Find the sum of all row vectors of input 
    rowsum = np.array(features.sum(1))
    # 1 / rowsum
    r_inv = np.power(rowsum, -1).flatten()
    # 1 / 0 is set as zero
    r_inv[np.isinf(r_inv)] = 0.
    # Row wise scaling by the normalization factor
    r_mat_inv = sp.diags(r_inv, 0)
    features = r_mat_inv.dot(features)
    return sparsematrix_to_tuple(features)


# Preprocessing of adjacency matrix for simple GCN model 
# and conversion to tuple representation
def preprocess_adj(adj):
    adj_normalized = normalize_adj(adj + sp.eye(adj.shape[0]))
    return sparsematrix_to_tuple(adj_normalized)


# Construct feed dictionary
def construct_feed_dict(features, support, labels, labels_mask, placeholders):
    feed_dict = dict()
    feed_dict.update({placeholders['labels']: labels})
    feed_dict.update({placeholders['labels_mask']: labels_mask})
    feed_dict.update({placeholders['features']: features})
    feed_dict.update({placeholders['support'][i]: support[i] for i in range(len(support))})
    feed_dict.update({placeholders['num_features_nonzero']: features[1].shape})
    return feed_dict


# Glorot and Bengio initialization scheme (AISTATS 2010)
def glorot(shape, name=None):
    init_range = np.sqrt(6.0/(shape[0]+shape[1]))
    initial = tf.random_uniform(shape, minval=-init_range, maxval=init_range, dtype=tf.float32)
    return tf.Variable(initial, name=name)


# Softmax cross-entropy loss with masking
def masked_softmax_cross_entropy(preds, labels, mask):
		# Calculate softmax cross entropy (tensorflow library)
    loss = tf.nn.softmax_cross_entropy_with_logits(logits=preds, labels=labels)
    mask = tf.cast(mask, dtype=tf.float32)
    # Normalize
    mask /= tf.reduce_mean(mask)
    loss *= mask
    return tf.reduce_mean(loss)


# Accuracy with masking
def masked_accuracy(preds, labels, mask):
    correct_prediction = tf.equal(tf.argmax(preds, 1), tf.argmax(labels, 1))
    accuracy_all = tf.cast(correct_prediction, tf.float32)
    mask = tf.cast(mask, dtype=tf.float32)
    mask /= tf.reduce_mean(mask)
    accuracy_all *= mask
    return tf.reduce_mean(accuracy_all)
