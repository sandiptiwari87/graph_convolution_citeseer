# Semi-Supervised Learning using Graph Convolutions on Citeseer Dataset

## How to run
```bash
python2 main.py
```

## Links
- `https://sandiptiwari87@bitbucket.org/sandiptiwari87/graph_convolution_citeseer.git`
- `https://arxiv.org/abs/1609.02907`
