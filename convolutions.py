# Team : Support_Vectors 
# Sandip Tiwari (20162032)
# Nagaraj Poti (20162010)
# Vatsal Nagda (20162008)
# Sachin Lomte (20162065)

from utils import *
import tensorflow as tf

# Dropout for sparse tensors
# Dropout consists in randomly setting a fraction rate of input units
# to 0 at each update during training time, which helps prevent overfitting
def sparse_dropout(x, keep_prob, noise_shape):
    random_tensor = keep_prob + tf.random_uniform(noise_shape)
    dropout_mask = tf.cast(tf.floor(random_tensor), dtype=tf.bool)
    pre_out = tf.sparse_retain(x, dropout_mask)
    return pre_out * (1.0/keep_prob)


# Wrapper for tf.matmul (sparse vs dense)
def dot(x, y, sparse):
    if sparse:
        res = tf.sparse_tensor_dense_matmul(x, y)
    else:
        res = tf.matmul(x, y)
    return res


# Graph convolution layer
class GraphConvolution():
    layer_uid = 1
    
    def __init__(self, input_dim, output_dim, placeholders,
                 sparse_inputs=False, act=tf.nn.relu):
				# The name of the current layer in the neural network
        self.name = 'layer_' + str(GraphConvolution.layer_uid)
        GraphConvolution.layer_uid += 1
        self.vars = {}
        self.dropout = placeholders['dropout']
        self.act = act
        self.support = placeholders['support']
        self.sparse_inputs = sparse_inputs

        with tf.variable_scope(self.name + '_vars'):
            for i in range(len(self.support)):
                self.vars['weights_' + str(i)] = glorot([input_dim, output_dim],
                                                        name='weights_' + str(i))


    def __call__(self, inputs, placeholders):
        with tf.name_scope(self.name):
            if not self.sparse_inputs:
                tf.summary.histogram(self.name + '/inputs', inputs)
            x = inputs
            # Determine amount of dropout in the input vector
            if self.sparse_inputs:
                x = sparse_dropout(x, 1-self.dropout, placeholders['num_features_nonzero'])
            else:
                x = tf.nn.dropout(x, 1-self.dropout)
            # convolve
            supports = list()
            for i in range(len(self.support)):
                pre_sup = dot(x, self.vars['weights_' + str(i)],
                                 sparse=self.sparse_inputs)
                support = dot(self.support[i], pre_sup, sparse=True)
                supports.append(support)
                output = tf.add_n(supports)

            return self.act(output)
